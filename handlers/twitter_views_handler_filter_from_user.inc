<?php
class twitter_views_handler_filter_from_user extends twitter_views_handler_filter {
  function query() {
    // Let the user allow to insert the username with @ and without @.
    $this->value = str_replace('@', '', $this->value);
    $this->query->add_where($this->options['group'], "from:$this->value");
  }
}

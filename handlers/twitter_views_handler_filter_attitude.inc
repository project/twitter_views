<?php
/**
 * @file
 *   Filter by tweets by attitude.
 */

define('TWITTER_VIEWS_ATTITUDE_SAD', ':(');
define('TWITTER_VIEWS_ATTITUDE_HAPPY', ':)');
define('TWITTER_VIEWS_ATTITUDE_QUESTION', '?');

class twitter_views_handler_filter_attitude extends twitter_views_handler_filter {
  function option_definition() {
    $options = parent::option_definition();

    return $options;
  }

  function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }

    $output = $this->options['value'];

    return $output;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Tweet attitude'),
      '#options' => array(
        '' => t('None'),
        TWITTER_VIEWS_ATTITUDE_SAD => t('Sadness'),
        TWITTER_VIEWS_ATTITUDE_HAPPY => t('Happyness'),
        TWITTER_VIEWS_ATTITUDE_QUESTION => t('Question'),
      ),
      '#default_value' => $this->options['value'],
    );
  }

  function query() {
    if ($this->value) {
      $this->query->add_where($this->options['group'], "$this->value");
    }
  }
}
